@extends('layout.app')
@section('content')

<div class="row">
	<div class="col-sm-4">
		<div class="half-unit">
			<dtitle>Баланс</dtitle><hr>
			<div ea-s='ta:c'> <digiclock>{{ $client->balance }} руб.</digiclock> </div>
		</div>
		<div class="half-unit">
			<dtitle>Пополнить</dtitle><hr>
			<div ea-s='ta:c'>
				{!! Form::open(['url' => '/balance-depos']) !!}
				<div class="form-group" ea-s='w:70p m:x:a'>
					{!! Form::number('sum',  '10', ['class' => 'form-control', 'required' => '']) !!}
					<button ea-j='mt=6px' type="submit" class="btn btn-success">Отправить</button>
				</div>
				{!! Form::close() !!}

			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="dash-unit">
			<dtitle>Клиент</dtitle>
			<hr>
			<div class="thumbnail">
				<img src="http://www.prepbootstrap.com/Content/images/shared/single-page-admin/face80x80.jpg" alt="Marcel Newman" class="img-circle">
			</div><!-- /thumbnail -->
			<h1>{{ $client->getUser()->full_name }}</h1>
			<h3>{{ $client->getNumber() }}</h3>
			<br>
			<div class="info-user">
				<a href="/my-services"><span aria-hidden="true" class="li_stack fs1"></span></a>
				<a href="/all-services"><span aria-hidden="true" class="li_note fs1"></span></a>
			</div>
		</div>
	</div>
	<div class="col-sm-4">
		<div class="dash-unit">
			<dtitle>Дополнительные сведения</dtitle>
			<hr>
			<div class="cont">
				<p><bold>{{ $client->getMonthServs() }} руб.</bold> | <ok>Оплата в месяц</ok></p>
				<br>
				<p><bold>{{ $client->getCountServs() }}</bold> | Подключенные услуги</p>
				<br>
				<p><bold>0 руб.</bold> | <bad>Долг</bad></p>
				<br>
			</div>
		</div>
	</div>
</div>

@endsection

