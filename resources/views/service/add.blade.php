@extends('layout.app')
@section('content')

<div class="col-lg-8 col-lg-offset-2" ea-s='m:b:big'>
	<h2>Нова услуга</h2>
	<br>
	<br>
	@include('service._form')
</div>
@endsection
