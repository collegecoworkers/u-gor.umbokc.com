{!! Form::open(['url' => isset($model) ? '/service/update/'.$model->id : '/service/create/' ]) !!}

	<div class="form-group">
		<label>Имя:</label>
		{!! Form::text('title', isset($model) ? $model->title : '', ['required' => '','class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Описание:</label>
		{!! Form::textarea('desc', isset($model) ? $model->desc : '', ['required' => '','class' => 'form-control']) !!}
	</div>

	<div class="form-group">
		<label>Цена:</label>
		{!! Form::number('price', isset($model) ? $model->price : '', ['required' => '','class' => 'form-control']) !!}
	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-success">Отправить</button>
	</div>
{!! Form::close() !!}

