@extends('layout.app')
@section('content')

<div class="row">
	<h2>Услуги</h2>
	<div class="" ea-s='m:b'>
		<a href="/service/add" class="btn btn-primary">Добавить</a>
	</div>
	<table class="table table-index">
		<thead>
			<tr>
				<th>Название</th>
				<th>Описание</th>
				<th>Стоимость</th>
				<th>Действие</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($services as $item)
			<tr>
				<td>{{ $item->title }}</td>
				<td> {{ $item->desc }}</td>
				<td>{{ $item->price }} руб.</td>
				<td>
					<a href="/service/edit/{{ $item->id  }}">
						<i class="li_pen"></i>
					</a>
					<a href="/service/delete/{{ $item->id  }}" onclick="return confirm('Вы уверенны?')">
						<i class="li_trash"></i>
					</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection
