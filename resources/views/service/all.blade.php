@extends('layout.app')
@section('content')

<div class="row">
	@if ($its_my)
		<h2>Мои услуги</h2>
	@else
		<h2>Все услуги</h2>
	@endif
	<table class="table table-index">
		<thead>
			<tr>
				<th>Название</th>
				<th>Описание</th>
				<th>Стоимость</th>
				@if (!$its_my)
					<th>Действие</th>
				@endif
			</tr>
		</thead>
		<tbody>
			@foreach ($services as $item)
			<tr>
				<td>{{ $item->title }}</td>
				<td> {{ $item->desc }}</td>
				<td>{{ $item->price }} руб.</td>
				@if (!$its_my)
					<td>
						@if ($item->curr_has())
							<p>Услуга уже подключена</p>
						@else
							<a href="/service/enable/{{ $item->id }}">Подключить</a>
						@endif
					</td>
				@endif
			</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection
