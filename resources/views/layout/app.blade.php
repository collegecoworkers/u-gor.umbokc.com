@php
use App\User;
@endphp
<!DOCTYPE html>
<html lang="en" ea>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
	
	<link rel="stylesheet" type="text/css" href="/assets/bootstrap/css/bootstrap.min.css" />
	<link href="/assets/css/main.css" rel="stylesheet">
	<link href="/assets/css/styles.css" rel="stylesheet">
	<link href="/assets/css/table.css" rel="stylesheet">
	<link href="http://www.prepbootstrap.com/Content/css/single-page-admin/font-style.css" rel="stylesheet">

	<script src="https://code.jquery.com/jquery-1.10.2.min.js" integrity="sha256-C6CB9UYIS9UJeqinPHWTHVqh/E1uhG5Twh+Y5qFQmYg=" crossorigin="anonymous"></script>
	
	<style type="text/css">body {padding-top: 60px;}</style>

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">


	<link rel="stylesheet" href="http://cdn.umbokc.com/ea/src/ea.css?v=2">
	<script src="http://cdn.umbokc.com/ea/src/ea.js?v=2"></script>
</head>
<body>

	<div class="navbar-nav navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">{{ config('app.name', 'Laravel') }}</a>
			</div> 
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li><a href="/" >Главная</a></li>
					<li><a href="/my-services" >Мои услуги</a></li>
					<li><a href="/all-services" >Все услуги</a></li>
				</ul>
				<ul class="nav navbar-nav pull-right">
					@if (User::isAdmin())
						<li><a href="/service/all" >Услуги</a></li>
						<li><a href="/user/all" >Пользователи</a></li>
					@endif
					<li>
						<form id="logout-form" action="/logout" method="POST" style="display: none;">{{ csrf_field() }}</form>
						<a class="logout" href="/logout" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Выйти</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="container">
		@yield('content')
	</div>
	<div id="footerwrap">
		<footer class="clearfix"></footer>
		<div class="container">
			<div class="row">
				<div class="col-sm-12 col-lg-12">
					<p>&copy; {{ config('app.name', 'Laravel') }}. Все права защищены.</p>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
