<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">

	<title>{{ config('app.name', 'Laravel') }}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="viewport" content="width=device-width" />
	
	<link rel="stylesheet" href="/assets/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" href="/assets/css/login.css" />

	<style type="text/css">body {padding-top: 100px;}</style>

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
	<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">

</head>
<body style="background:url('http://www.prepbootstrap.com/Content/images/shared/single-page-admin/bg.jpg') no-repeat center center; height:700px;">

	<div class="container">
		<div class="row">
			@yield('content')
		</div>
	</div>

</body>
</html>
