@extends('layout.app')
@section('content')

<div class="row">
	<h2>Пользователи</h2>
	<table class="table table-index">
		<thead>
			<tr>
				<th>#</th>
				<th>Логин</th>
				<th>Email</th>
				<th>Роль</th>
				<th>Действия</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($users as $item)
				<tr>
					<td ta:c>{{$item->id}}</td>
					<td ta:c>{{$item->name}}</td>
					<td ta:c>{{$item->email}}</td>
					<td ta:c>{{$item->getRole()}}</td>
					<td>
						<a href="/user/edit/{{ $item->id  }}">
							<i class="li_pen"></i>
						</a>
						<a href="/user/delete/{{ $item->id  }}" onclick="return confirm('Вы уверенны?')">
							<i class="li_trash"></i>
						</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</div>

@endsection
