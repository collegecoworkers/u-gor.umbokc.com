@extends('layout.app')
@section('content')

<div class="col-lg-8 col-lg-offset-2" ea-s='m:b:big'>
	<h2>Изменить пользователя</h2>
	<br>
	<br>
	@include('user._form')
</div>

@endsection
