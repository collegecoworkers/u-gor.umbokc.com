<?php

Auth::routes();

Route::get('/', 'SiteController@Index');
Route::get('/my-services', 'SiteController@MyServices');
Route::get('/all-services', 'SiteController@AllServices');
Route::get('/service/enable/{id}', 'SiteController@EnableService');
Route::post('/balance-depos', 'SiteController@BalanceDepos');

// admin
Route::get('/service/all', 'ServiceController@All');
Route::get('/service/add', 'ServiceController@Add');
Route::post('/service/create', 'ServiceController@Create');
Route::get('/service/edit/{id}', 'ServiceController@Edit');
Route::post('/service/update/{id}', 'ServiceController@Update');
Route::get('/service/delete/{id}', 'ServiceController@Delete');

Route::get('/user/all', 'UserController@Index');
Route::get('/user/edit/{id}', 'UserController@Edit');
Route::get('/user/delete/{id}', 'UserController@Delete');
Route::post('/user/update/{id}', 'UserController@Update');
