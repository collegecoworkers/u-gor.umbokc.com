<?php
namespace App;

class Service extends MyModel{

	static function getsMy() {
		$es = EnableService::getsBy('user_id', User::id());
		$all_es_id = F::getField($es, 'service_id');
		return Service::whereIn('id', $all_es_id)->get();
	}

	function curr_has() {
		return EnableService::getCount(['service_id' => $this->id, 'user_id' => User::id()]) > 0;
	}

}
