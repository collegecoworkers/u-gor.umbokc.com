<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Client,
	Service,
	EnableService,
	User
};

class SiteController extends Controller
{

	function __construct(){
    $this->middleware('auth');
	}

	function Index() {
		$client = Client::curr();
		return view('index')->with([
			'client' => $client,
		]);
	}

	function MyServices() {
		$services = Service::getsMy();
		return view('service.all')->with([
			'services' => $services,
			'its_my' => true,
		]);
	}

	function AllServices() {
		$services = Service::all();
		return view('service.all')->with([
			'services' => $services,
			'its_my' => false,
		]);
	}

	function EnableService($id) {
		$service = Service::getById($id);
		$client = Client::curr();

		$enable_serv = new EnableService;
		$enable_serv->service_id = $service->id;
		$enable_serv->user_id = $client->user_id;

		if($client->balance > $service->price){
			$client->balance -= $service->price;
			$client->save();
			$enable_serv->save();
			return redirect('/my-services');
		}

		return redirect()->back();
	}

	function BalanceDepos(Request $r){
		$client = Client::curr();
		$client->balance += intval(request()->sum);
		$client->save();
		return redirect()->back();
	}

}
