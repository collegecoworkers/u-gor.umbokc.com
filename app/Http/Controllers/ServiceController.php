<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\{
	F,
	Service,
	User
};

class ServiceController extends Controller {

	function __construct(){
		$this->middleware('auth');
	}

	function All() {
		$services = Service::all();
		return view('service._all')->with([
			'services' => $services,
		]);
	}
	function Add() {
		return view('service.add');
	}
	function Edit($id) {
		$service = Service::getById($id);
		return view('service.edit')->with([
			'model' => $service,
		]);
	}
	function Create(Request $request) {
		$model = new Service();

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->price = request()->price;

		$model->save();
		return redirect('/service/all');
	}
	function Update($id, Request $request) {
		$model = Service::getById($id);

		$model->title = request()->title;
		$model->desc = request()->desc;
		$model->price = request()->price;

		$model->save();
		return redirect('/service/all');
	}
	function Delete($id) {
		Service::where('id', $id)->delete();
		return redirect()->back();
	}
}
