<?php
namespace App;

class Client extends MyModel{

	function getNumber() {
		$n = (string) $this->number;
		$n = substr_replace($n, ' ', 3, 0);
		return $n;
	}

	function getMonthServs(){
		$es = EnableService::getsBy('user_id', User::id());
		$sum = 0;

		$all_es_id = F::getField($es, 'service_id');
		$ss = Service::whereIn('id', $all_es_id)->get();
		$all_s = F::getField($ss, 'price');
		// dbg($all_s);
		$sum = array_sum($all_s);
		return $sum;
	}

	function getCountServs(){
		return EnableService::getCount('user_id', User::id());
	}

	function getUser() {
		return User::getById($this->user_id);
	}

	static function genNumber() {
		$n = mt_rand(1000000, 9999999);

		if (self::getCount('number', $n) > 0) {
			return self::genNumber();
		}
		return $n;
	}

	static function curr() {
		return self::getBy('user_id', User::id());
	}

	static function defBalance() {
		return 30000;
	}

}
